import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HeroeModel } from '../../models/heroe.model';
import { HeroeService } from '../../services/heroe.service';

import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe = new HeroeModel();

  constructor(private heroesService: HeroeService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {

    const id = this.route.snapshot.paramMap.get('id');

    if (id !== 'nuevo') {
      this.heroesService.getHeroe(id).subscribe ( (resp: HeroeModel)  => {
        this.heroe = resp;
        this.heroe.id = id;
      });
    }
  }

  guardar(form: NgForm) {

    if (form.invalid) {
      console.log('Formulario no valido!');
      return;
    }

    // Swal.fire({
    //   title: 'Espere',
    //   text: 'Guardando información',
    //   type: 'info',
    //   allowOutsideClick: false
    // });

    Swal
    .fire({
        title: 'Espere',
        text: 'Guardando informacion',
        icon: 'info',
        allowOutsideClick: false
        // showCancelButton: true,
        // confirmButtonText: "Sí, eliminar",
        // cancelButtonText: "Cancelar",
    });

    Swal.showLoading();

    let peticion: Observable<any>;

    if (this.heroe.id) {
      // this.heroesService.actualizarHeroe(this.heroe).subscribe(
      //   resp => console.log(resp)
      // );
      peticion = this.heroesService.actualizarHeroe(this.heroe);
    } else {
      // this.heroesService.crearHeroe(this.heroe).subscribe(
      //   resp => console.log(resp)
      // );

      peticion = this.heroesService.crearHeroe(this.heroe);
    }

    peticion.subscribe( resp => {
      Swal
      .fire({
        title: this.heroe.nombre,
        text: 'Información actualizada con éxito',
        icon: 'success' // ,
        // allowOutsideClick: false
        // showCancelButton: true,
        // confirmButtonText: "Sí, eliminar",
        // cancelButtonText: "Cancelar",
    });
    });



  }



}
