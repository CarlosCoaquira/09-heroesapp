import { Component, OnInit } from '@angular/core';
import { HeroeService } from '../../services/heroe.service';
import { HeroeModel } from '../../models/heroe.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: HeroeModel[] = [];
  cargando = false;

  constructor(private heroeService: HeroeService) { }

  ngOnInit(): void {

    this.cargando = true;
    this.heroeService.getHeroes()
    .subscribe ( resp => {
      // console.log(resp);
      this.heroes = resp;
      this.cargando = false;
    });
  }

  borrarHeroe(heroe: HeroeModel, i: number) {

    Swal.fire({
      title: '¿Esta seguro?',
      text: `Dese elmininar al heroe ${ heroe.nombre }`,
      icon: 'question',
      allowOutsideClick: false,
      showCancelButton: true,
      showConfirmButton: true
    }).then( resp => {

      if (resp.value) {
        this.heroes.splice(i, 1); // borrar de arreglo
        this.heroeService.borrarHeroe(heroe.id)
        .subscribe();
      }
    });
  }

}
