import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeroeModel } from '../models/heroe.model';
import { map, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroeService {

  url = 'https://heroes-crud-39ef2.firebaseio.com';
  constructor(private http: HttpClient) { }

  crearHeroe(heroe: HeroeModel) {
    return this.http.post(`${ this.url }/heroes.json`, heroe).pipe(
      map( (resp: any) => {
        heroe.id = resp.name;
        return heroe;
      })
    );
  }

  actualizarHeroe(heroe: HeroeModel) {

    console.log('actualizar');

    const heroeTemp = {
      ...heroe // "..." clona todas las propiedas de la clase
    };
    console.log(heroeTemp);

    delete heroeTemp.id;


    console.log(heroeTemp);

    return this.http.put(`${ this.url }/heroes/${ heroe.id }.json`, heroeTemp);
  }

  borrarHeroe( id: string) {
    return this.http.delete(`${ this.url }/heroes/${ id }.json`);
  }

  getHeroe( id : string) {

    return this.http.get(`${ this.url }/heroes/${ id }.json`);
  }

  getHeroes() {

    return this.http.get(`${ this.url }/heroes.json`)
    .pipe(
      map(  this.crearArreglo), // esto es igual a: (resp => this.crearArreglo(resp) ). El map proporcionar su primer parametro
      delay(0)
    );
  }

  private crearArreglo(heroesObj: object) {

    const heroes: HeroeModel[] = [];
    // console.log(heroesObj);

    if (heroesObj === null ) { return []; }

    Object.keys( heroesObj ).forEach ( key => {
      // console.log(key);
      const heroe: HeroeModel = heroesObj[key];
      // console.log(heroe);
      heroe.id = key;

      heroes.push(heroe);

    } );

    return heroes;
  }
}
